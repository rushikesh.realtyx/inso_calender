<!-- <!DOCTYPE html>
<head>
<title>Form submission</title>
</head>
<body>

<form action="quickstart.php" method="post">
Summary: <input type="text" name="summary"><br>
location: <input type="text" name="location"><br>
Email:<br><textarea rows="5" name="email" cols="30"></textarea><br>

description:<br><textarea rows="5" name="description" cols="30"></textarea><br>
<input type="submit" name="submit" value="Submit">
</form>

</body>
</html> -->

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Add Event Form</h2>
	<form action="quickstart.php" method="post">
    <div class="form-group">
      <label for="summary">Summary:</label>
      <input type="text" class="form-control" id="summary" placeholder="Enter Summary" name="summary">
    </div>
    <div class="form-group">
      <label for="location">Location:</label>
      <input type="text" class="form-control" id="location" placeholder="Enter Location" name="location">
    </div>
    <div class="form-group">
      <label for="email">Email:</label>
      <textarea type="text" class="form-control" rows="5" id="email" name="email" placeholder="To Enter Multiple Email use comma"></textarea>
    </div>
    <div class="form-group">
      <label for="description">Description:</label>
      <textarea type="text" class="form-control" rows="5" id="description" name="description" placeholder="Enter Description"></textarea>
    </div>
	<input type="submit" name="submit" value="Submit">
   
  </form>
</div>

</body>
</html>
